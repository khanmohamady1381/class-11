package sbu.cs.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sbu.cs.gui.configuration.Config;

import java.io.IOException;
import java.net.URL;

public class PageLoader {
    private static final String TITLE_CONFIG_KEY = "stage.title";
    private static Stage stage;
    private static int WIDTH;
    private static int HEIGHT;

    static void intiStage(Stage primaryStage, Config config) {
        WIDTH = config.getIntValue("stage.width");
        HEIGHT = config.getIntValue("stage.height");
        stage = primaryStage;
        stage.setTitle(config.getStringValue(TITLE_CONFIG_KEY));
        stage.setResizable(config.getBooleanValue("stage.resizable"));
        stage.setWidth(WIDTH);
        stage.setHeight(HEIGHT);
    }


    public Scene load(String url) throws IOException {
        URL resource = getClass().getResource(url);
        System.out.println("resource url to " + url + " is " + resource);
        Parent root = FXMLLoader.load(resource);
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        stage.setScene(scene);
        stage.show();
        return scene;
    }

}