package sbu.cs.gui;

import javafx.application.Application;
import javafx.stage.Stage;
import sbu.cs.gui.configuration.Config;
import sbu.cs.gui.pages.Home;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Config config = new Config();
        PageLoader.intiStage(primaryStage, config);
        new Home(config);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
