package sbu.cs.gui.controller;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    private static int STEP = 5;
    private static HomeController instance;

    @FXML
    private ImageView heroIcon;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;

    }

    public static void setKeyListener(Scene scene) {
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                KeyCode code = event.getCode();
                System.out.println("code read was " + code);
                switch (code) {
                    case UP:
                        instance.goUp(null);
                        break;
                    case DOWN:
                        instance.goDown(null);
                        break;
                    case LEFT:
                        instance.goLeft(null);
                        break;
                    case RIGHT:
                        instance.goRight(null);
                        break;
                }
            }
        });
    }

    public static void setVariableConfig(int stepSize) {
        STEP = stepSize;
    }

    @FXML
    void goUp(ActionEvent event) {
        System.out.println("salam go up called");
        double y = heroIcon.getY();
        heroIcon.setY(y - STEP);
    }

    @FXML
    void goLeft(ActionEvent event) {
        double x = heroIcon.getX();
        heroIcon.setX(x - STEP);
    }

    @FXML
    void goRight(ActionEvent event) {
        double x = heroIcon.getX();
        heroIcon.setX(x + STEP);
    }

    @FXML
    void goDown(ActionEvent event) {
        double y = heroIcon.getY();
        heroIcon.setY(y + STEP);
    }
}
