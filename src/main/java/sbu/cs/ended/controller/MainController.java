package sbu.cs.ended.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private ImageView heroIcon;
    private static MainController instance;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;
    }

    public static void setListeners(Scene scene) {
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                KeyCode code = event.getCode();
//                System.out.println("code read was " + code);
                switch (code) {
                    case UP:
                        instance.goUp(null);
                        break;
                    case DOWN:
                        instance.goDown(null);
                        break;
                    case LEFT:
                        instance.goLeft(null);
                        break;
                    case RIGHT:
                        instance.goRight(null);
                        break;
                }
            }
        });
    }

    @FXML
    void newPage(ActionEvent event) {
        setListeners(heroIcon.getScene());
    }

    @FXML
    void goDown(ActionEvent event) {
        double y = heroIcon.getY();
        heroIcon.setY(y + 5);
    }

    @FXML
    void goLeft(ActionEvent event) {
        double y = heroIcon.getX();
        heroIcon.setX(y - 5);
    }

    @FXML
    void goRight(ActionEvent event) {
        double y = heroIcon.getX();
        heroIcon.setX(y + 5);
    }

    @FXML
    void goUp(ActionEvent event) {
        double y = heroIcon.getY();
        heroIcon.setY(y - 5);
    }

    @FXML
    void quit(ActionEvent event) {
        Platform.exit();
    }
}
